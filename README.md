# Simple Picture Sorter #

This is a simple script that will take all the photos from a directory, and moves them to another one, but sorts them in folders by date.

This was created as a Christmas Present for my mom who likes taking pictures, but needed an easy way to sort photos.

This will require python3.
