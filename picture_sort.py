#!/usr/bin/python3
import sys
import picture_sorter

if __name__ == "__main__":
    try:
        picture_sorter.main()
    except (KeyboardInterrupt):
        print("Caught ctrl+c, terminating...")
        sys.exit(0)

