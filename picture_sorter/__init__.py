import argparse
import glob
import os
import shutil
import time

from enum import IntEnum

class ErrorNumber(IntEnum):
    INPUT_DOESNT_EXIST = 100

class PictureSortException(Exception):
    def __init__(self, errorNumber, message):
        self.errorNumber = errorNumber
        self.message = message

    def what(self):
        return str(self.errorNumber) + ": " + self.message

def checkForDirs(inputDir, outputDir):
    '''
    @brief Checks if the given directories actually exist
    @param inputDir - The input directory, where the unsorted images exist
    @param outputDir - The output directory, where the images will be sorted to.
                       If the output directory does not exist, one will attempt
                       to make it.
    @returns true if both the input directory and output directory exists
    '''
    if (not os.path.isdir(inputDir)):
        raise PictureSortException (ErrorNumber.INPUT_DOESNT_EXIST, 
                                    "Input directory '" + inputDir + "' does not exist!")
    elif (not os.path.isdir(outputDir)):
        os.mkdir(outputDir)

def getImagesAndDates(inputDir):
    '''
    @brief Gets all the .jpg, .png, .gif, .tiff files in the input directory
    @param inputDir - Where all the images are located
    @returns a map whose keys are the date of images, and the value is a list 
             of the image's file names
    '''
    fileTypes = ['jpg', 'jpeg', 'png', 'gif', 'tiff']
    images = {}
    for type in fileTypes:
        files = glob.glob(os.path.join(inputDir, '*.' + type))
        for f in files:
            modTime = os.path.getmtime(f)
            timeStr = time.strftime('%m_%d_%y', time.localtime(modTime))
            try:
                images[timeStr] += [f]
            except KeyError:
                images[timeStr] = [f]

    return images

def moveImages(images, outputDir, copy=False):
    '''
    @brief actually moves the images to their folders
    @param images - the image map from getImagesAndDates()
    @param outputDir - where to move the images
    @param copy - Do not remove the old files, simply copy them
    '''
    for k,v in images.items():
        dateFolder = os.path.join(outputDir, k)
        if (not os.path.isdir(dateFolder)):
            print ("Making output directory " + dateFolder)
            os.mkdir(dateFolder)
        for file in v:
            output = os.path.join(outputDir, k, os.path.basename(file))
            if (copy):
                print ("Copying from: " + file + " to " + output)
                shutil.copy2(file, output)
            else:
                print ("Moving from: " + file + " to " + output)
                shutil.move(file, output)

def main():
    parser = argparse.ArgumentParser(description = "This is a simple script that will " + \
                                                   "take all the photos from a directory, " + \
                                                   "and moves them to another one, but sorts" + \
                                                   "them in folders by date.")

    parser.add_argument('input', type=str,
                        help="The directory to move the images from.")
    parser.add_argument('output', type=str,
                        help="The directory to move and sort the images to.")
    parser.add_argument('--copy', dest='copy',  action='store_true',
                        help='Use this argument if the old files should NOT be removed.')

    args = parser.parse_args()

    inputDir = args.input
    outputDir = args.output
    copy = args.copy
    try:
        checkForDirs(inputDir, outputDir)
        images = getImagesAndDates(inputDir)
        moveImages(images, outputDir, copy)

    except PictureSortException as e:
        print (e.what())
        exit(e.errorNumber)

